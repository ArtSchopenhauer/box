import json
import requests
from PyPDF2 import PdfFileWriter

# permanent values
client_id = "geugjjmpvsysqxx3kdxp7r77hika0yhm"
client_secret = "Cg31t7GJTtoe2tgdcZjLFZjTpsJKHrcq"

# get refresh_token, to use to get access_token
token_file = open("token.json", "r")
token_json = json.load(token_file)
refresh_token = token_json["refresh_token"]
token_file.close()

# to get access_token and new refresh_token, POST to this url with refresh_token (and 3 other body parameters)
get_token_url = "https://app.box.com/api/oauth2/token"
body = {"grant_type": "refresh_token",
		"refresh_token": refresh_token,
		"client_id": client_id,
		"client_secret": client_secret}

# get access_token and new refresh_token
response_object = requests.post(get_token_url, data=body)
response_body = response_object.json()
access_token = response_body["access_token"]

# store refresh_token in json file on hard drive
file_object = open("token.json", "w")
json.dump(response_body, file_object)
file_object.close()

"""
### navigate through folders

# url to get sub-folders of NY Customer Database
folder_id = "1763301834"
folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"

# using access_token, get sub-folders of NY Customer Database
headers = {"Authorization": "Bearer " + access_token}
folder_items = requests.get(folder_items_url, headers=headers).json()

for item in folder_items["entries"]:
	if item["name"] == "Nassau":
		folder_id = item["id"]
		folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"

folder_items = requests.get(folder_items_url, headers=headers).json()

### download file

file_id = "39907367049"
download_file_url = "https://api.box.com/2.0/files/" + file_id + "/content"

headers = {"Authorization": "Bearer " + access_token}
r = requests.get(download_file_url, headers=headers)
data = r.content

customer = "Slupek"

pdf_file = open("%s 5 Minute Test.pdf" % customer, "wb")
pdf_file.write(data)
pdf_file.close()

"""