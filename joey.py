#box.py - build folder

from flask import Flask, jsonify, render_template, request, json, redirect
import requests
import os
import json

app = Flask(__name__, static_url_path='/static')

def doit(url, headers, payload):
    r = requests.post(url, headers = headers, data = json.dumps(payload))
    return r
    
def add_collab_install(folderid, headers):
    url = 'https://api.box.com/2.0/collaborations'
    #lipostinstall
    payload = {'item': { 'id': folderid, 'type': 'folder'}, 'accessible_by': { 'id': '253388741', 'type': 'user' }, 'role': 'editor'}
    requests.post(url, headers = headers, data = json.dumps(payload))
    return;

def build(token):
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url =  os.path.join(SITE_ROOT, 'static', 'account.json')    
    info = open(json_url)
    stored_json = json.load(info)
    first = stored_json['first']
    last = stored_json['last']
    county = stored_json['county']
    city = stored_json['city']
    custnum = stored_json['custnum']
    name = last + ', ' + first
    foldername = custnum + ' - ' + name
    headers =  {'Authorization': 'Bearer ' + token}
    
    url = 'https://api.box.com/2.0/folders/1763301834/items'
    json_r = requests.get(url, headers=headers).json()
    for item in json_r['entries']:
        if item['name'] == county:
            county_id = item['id']
    url = 'https://api.box.com/2.0/folders/' + county_id + '/items'
    json_r = requests.get(url, headers=headers).json()

    for item in json_r['entries']:
        if item['name'] == city:
            city_id = item['id']

    url = 'https://api.box.com/2.0/folders'
    payload = {'name': foldername, 'parent': {'id': city_id}}
    customer_root_id = doit(url, headers, payload).json()['id']

    payload = {'name':name + ' - Array Design', 'parent': {'id': customer_root_id}}
    customer_ad_id = doit(url, headers, payload).json()['id']

    payload = {'name':name + ' - Install Plan','parent': {'id' : customer_root_id}}
    customer_install_id = doit(url, headers, payload).json()['id']
    add_collab_install(customer_install_id, headers)

    payload = {'name':name + ' - Permits','parent': {'id' : customer_root_id}}
    customer_pt_id = doit(url, headers, payload).json()['id']

    payload = {'name':name + ' - Site Survey','parent': {'id' : customer_install_id}}
    customer_ss_id = doit(url, headers, payload).json()['id']

    payload = {'name':name + ' - Post-Install Photos','parent': {'id' : customer_install_id}}
    customer_post_id = doit(url, headers, payload).json()['id']
    add_collab_install(customer_post_id, headers)

    payload = {'name':name + ' - Fund Documents','parent': {'id' : customer_pt_id}}
    customer_fund_id = doit(url, headers, payload).json()['id']

    url = 'https://api.box.com/2.0/collaborations'

    #Site Survey
    payload = {'item': { 'id': customer_ss_id, 'type': 'folder'}, 'accessible_by': { 'id': '213804720', 'type': 'user' }, 'role': 'editor'}
    doit(url, headers, payload) 
    return "done"

@app.route("/build/")    
def token_verify():
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
        json_url =  os.path.join(SITE_ROOT, 'static', 'box.json')    
        token = open(json_url)
        stored_json = json.load(token)
        token.close()
        refresh = stored_json['refresh']
        access = stored_json['access']
        url = 'https://api.box.com/2.0/folders/1763301834/items'
        headers =  {'Authorization': 'Bearer ' + access}
        json_r = requests.get(url, headers=headers)

        if json_r.status_code != 200:
            url = 'https://api.box.com/oauth2/token'
            payload = {'grant_type':'refresh_token','refresh_token':refresh,'client_id':'2jo785briaim4k39h64qs01ey46n2uag','client_secret':'y2HDXknQy5i19HLTWCOaTBBPImZ4J6kW'}
            json_r = requests.post(url, data=payload)  
            if json_r.status_code != 200:
                return redirect("https://app.box.com/api/oauth2/authorize?response_type=code&client_id=2jo785briaim4k39h64qs01ey46n2uag&state=security_token%3316J0oeiy", code=302)
            else:
                json_r = json_r.json()
                msg = {"refresh": json_r['refresh_token'], "access": json_r['access_token']}
                box = open(json_url,'w')
                stored_json = json.dump(msg,box)
                box.close()
                build(str(json_r['access_token']))

        else:
            return build(str(access))
    except Exception as e:
        return str(e)
    
@app.route('/folder', defaults={'code':None}, methods=['GET', 'POST'])
@app.route("/folder/<code>", methods=["GET", "POST"])
def search(code):
    try:
        if request.method == "POST":
            url = "https://levelsolar.secure.force.com/api/services/apexrest/accounts?account_number=" + request.form["user_search"]
            response_dict = requests.get(url).json()
            url = 'https://levelsolar.secure.force.com/api/services/apexrest/contacts?account=' + response_dict[0]["id"]
            response_dict = requests.get(url).json()
            city = response_dict[0]['city']
            first = response_dict[0]['first_name']
            last = response_dict[0]['last_name']
            SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
            json_url = os.path.join(SITE_ROOT, 'static', 'account.json')
            county = response_dict[0]['account']['municipality']['county']['name']
            msg = {"city": city, "first": first, "last": last, "county": county, "custnum": request.form["user_search"]}
            account = open(json_url,'w')
            stored_json = json.dump(msg, account)
            account.close()
            return redirect("http://ad.universalanswer.com/build/", code=302)
        elif code != None:
            payload = {'grant_type':'authorization_code','code':code,'client_id':'2jo785briaim4k39h64qs01ey46n2uag','client_secret':'y2HDXknQy5i19HLTWCOaTBBPImZ4J6kW'}
            url = 'https://api.box.com/oauth2/token'
            json_r = requests.post(url, data=payload).json()
            SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
            json_url =  os.path.join(SITE_ROOT, 'static', 'box.json')
            msg = {'refresh': json_r['refresh_token'], 'access': json_r['access_token']}
            box = open(json_url,'w')
            stored_json = json.dump(msg,box)
            return redirect("http://ad.universalanswer.com/build/", code=302)
        else: # request.method == "GET"
            return render_template("front.html")
    except Exception as e:
        return str(e)
    


